from flask import Flask, request, jsonify


app = Flask(__name__)


def make_error(code, message):
    err = {
        'error': {
            'code': code,
            'message': message
        },
        'status': 'Error',
        'data': ''
    }
    return jsonify(err), code


def create_pass_db(data):
    return {
        'status': 'Ok'
    }


@app.route('/push', methods=['POST'])
def push():
    data = request.json
    if data and all([field in data for field in ['serial_number_id', 'parent_serial_number_id']]):
        return create_pass_db(data)
    else:
        return make_error(400, 'serial_number_id and parent_serial_number_id are required.')


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True)